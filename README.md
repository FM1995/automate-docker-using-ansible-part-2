# Automate Docker using Ansible Part 2

#### Project Outline

This is a continuation from Part 1 so in Ansible we can use modules and plugins to work with Docker. This gives us state management where Ansible is able to know whether to execute again, where as with command and shell it does not have state management.

#### Lets get started

So if we go back to Ansible documentation page

https://docs.ansible.com/ansible/latest/collections/community/docker/docker_image_module.html

![Image 1](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image1.png)

The community.docker.docker_image module in Ansible is a tool for managing Docker images, enabling users to perform operations such as building, loading, pulling, pushing, and tagging images. It is included within the community.docker collection that provides modules and plugins for Docker interactions. For detailed usage instructions, including available parameters, example usages, and expected return information, users should consult the official documentation page.

Now utilising the below

![Image 2](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image2.png)

Can configure it like the below

![Image 3](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image3.png)

However upon running the playbook again, can obtain the below error

```
ansible-playbook deploy-docker.yaml
```

We get the below error

![Image 4](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image4.png)

The reason for the above error, I haven't installed the Python requests module, which is necessary for the Docker SDK for Python. This SDK is crucial for carrying out Docker-related operations through Ansible on my target machine, which is using Python 3 and is located at the IP address 18.196.100.76. I need to ensure that I install the most up-to-date version of the Docker SDK that is compatible with Python 3.

To make it easier to understand, imagine Ansible as a robot that I've programmed to manage Docker containers on a remote machine. For the robot to function correctly, it needs specific tools, which in this scenario are Python modules. It attempted to complete a task but failed because it lacked an essential tool, the requests module, required for interacting with Docker's components. Even though my initial setup installed Python, it overlooked adding the necessary Python modules, requests and docker, which are needed for Docker tasks. I can fix this issue by including steps to install these modules.

To solve this problem, I can refer to the documentation to install the Python Docker module, which should address the error I'm encountering.

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pip_module.html

![Image 5](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image5.png)

NOTE!!! due to technicalities I had to switch from the ec2 instance from Amazon Linux to Ubuntu and therefore use apt

![Image 6](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image6.png)

And also add the new the new user ‘ubuntu’

![Image 7](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image7.png)

Can then run the ansible command

```
ansible-playbook deploy-docker.yaml
```

And can see it was successful, where docker python was installed and it was able to pull the redis image using 

![Image 8](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image8.png)


Next lets start the docker containers, where we will pull a docker image from our repository, set the environmental variables for the database and deploy a mysql database. But first lets copy the docker-compose file from our host to the ubuntu server.

Just like the below.

Let’s prepare the format for the docker compose to be copied over


![Image 9](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image9.png)

Finalised copy for docker compose

![Image 10](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image10.png)

Can then configure the docker-compose file with the necessary details, such as image name, environmental variables for the database and mysql database.

Can see the image name on the repository

![Image 11](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image11.png)

Below is the docker-compose file

![Image 12](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image12.png)

Now we need ansible to login to docker hub and fetch that image

Can use the below module

https://docs.ansible.com/ansible/latest/collections/community/docker/docker_login_module.html

![Image 13](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image13.png)

Can then configure it like the below, where the password is a variable and the registry url for docker is specified.

![Image 14](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image14.png)

Instead of saving it as a variable we can also set it as a prompt in the vars section

![Image 15](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image15.png)


Can then execute the playbook again

And can see the enter password prompt in the playbook output

![Image 16](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image16.png)

And can see when pulling my docker image it is successful

![Image 17](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image17.png)

Instead of the prompt we can also set the password as a variable

![Image 18](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image18.png)


Can run it again and see the output

![Image 19](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image19.png)


Now going back to the ec2 server

Can see docker compose is available and the previously obtained pulled images

![Image 20](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image20.png)


Now we want to execute the docker-compose in the ec2 instance using ansible and we can do that using

We can implement the below with the location of the docker-compose file, can also include the state which is present to utilise docker-compose up or absent which docker compose down


![Image 21](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image21.png)


Now ready to run, however getting the below

![Image 22](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image22.png)


To fix it we can include the below of docker-compose in the installation of docker python module

![Image 23](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image23.png)


Now ready to run the playbook again

```
ansible-playbook deploy-docker.yaml
```

Can see docker compose ran successfully

![Image 24](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image24.png)

And can see the can see the containers have succesfully started

![Image 25](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image25.png)


Now lets do it on a completely new server to test the steps

Lets first destroy this ec2-instance

```
terraform destroy -auto-approve
```

![Image 26](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image26.png)

Now lets create a new one

```
terraform apply –auto-approve
```

Can see the resource got created

![Image 27](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image27.png)

Updating the hosts

![Image 28](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image28.png)

Sometimes we may need to set attributes to gather facts to false for first play, this is because on a fresh new server ansible will give an error at this part even if we set it to use python2 for first play so because of this set it to false to skip the first gathering facts since we have defined to use python3 in the ansible configuration

![Image 29](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image29.png)

Hence doing the below change will resolve that since we don’t have python3 installed on the first play or even the new server

![Image 30](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image30.png)

Now ready to execute our playbook so that it can run the docker containers

And can see success with the new containers running

![Image 31](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image31.png)

One thing to note is that the playbook will not work on different linux distributions, to resolve that we can make our playbook re-usable as possible
One thing we can do is to create a new user to execute the playbook instead of the provided ec2 user. We will create new user ‘fuad’ just like the below and assign it to the admin/docker group. Another advantage of creating just the user is that there is no need to reset the connection 

![Image 32](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image32.png)

Can then configure the below

![Image 33](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image33.png)

Aswell as changes to the destination

![Image 34](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/raw/main/Images/Image34.png)









